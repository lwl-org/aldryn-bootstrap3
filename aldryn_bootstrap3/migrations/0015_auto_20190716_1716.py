from django.db import migrations
import django.db.models.deletion
import filer.fields.folder


class Migration(migrations.Migration):

    dependencies = [
        ('aldryn_bootstrap3', '0014_translations_update'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bootstrap3carouselslidefolderplugin',
            name='folder',
            field=filer.fields.folder.FilerFolderField(on_delete=django.db.models.deletion.CASCADE, to='filer.Folder', verbose_name='Folder'),
        ),
    ]
