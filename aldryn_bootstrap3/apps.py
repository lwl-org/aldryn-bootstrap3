from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class AldrynBootstrap3Config(AppConfig):
    name = 'aldryn_bootstrap3'
    verbose_name = _("Bootstrap3")
